FROM mhart/alpine-node:12
COPY . /api/
WORKDIR /api
RUN npm install
EXPOSE 8443/tcp
CMD ["npm", "start"]
