'use strict'

const express = require('express');
const gateway = require("fast-gateway");
const multipleHooks = require('fg-multiple-hooks');
const toArray = require('stream-to-array');
var compression = require('compression')
var jwt = require('jsonwebtoken');
const got = require('got');
var cors = require('cors')

const fs = require('fs');
const gatewayKey  = fs.readFileSync('gateway.key', 'utf8');
const gatewayCrt = fs.readFileSync('gateway.crt', 'utf8');
const credentials = { key: gatewayKey, cert: gatewayCrt };

const PORT = process.env.PORT || 8443;
const env = require('./environment').default;

const usersAPI = env.getProperty('api.users');
const sessionsAPI = env.getProperty('api.sessions');
const accountsAPI = env.getProperty('api.accounts');

const bmxToken = env.getProperty('bmx.token');

const enforceAuthorization = async (req, res) => {
    const authorization = req.headers['authorization'];
    var token = "", userData, logoutPath;

    if( !authorization ) {
        res.status(401).end(JSON.stringify({ error: 'unauthorized' }));
        return true;
    }

    token = authorization.split(" ");

    try {
        userData = jwt.verify(token[1], 'shhhhh');
    } catch(e) {

        userData = jwt.decode(token[1]);

        if (e instanceof jwt.JsonWebTokenError) {

            if( e.name == 'TokenExpiredError' ) {
                logoutPath = `/logout/${userData.uid}/${userData.sid}`
                let logoutResponse = await got.delete(`${sessionsAPI}${logoutPath}`).json();

                if( logoutPath == req.path ) {
                    res.status(200).end(JSON.stringify(logoutResponse));     
                } else {
                    res.status(401).end(JSON.stringify({ error: e.name }));
                }
            }

            res.status(401).end(JSON.stringify({ error: e.name }));
			return true;
        }

        res.status(400).end(JSON.stringify({ error: e.message }));
        return true;
    }

    req.headers['proxy-authorization'] = JSON.stringify({
        uid: userData['uid'], 
        sid: userData['sid'], 
        email: userData['email']
    });

    logoutPath = `/logout/${userData.uid}/${userData.sid}`

    if( req.path == logoutPath ) { 
        return false; 
    }

    const tokenStatus = await got(`${sessionsAPI}/login/${userData.uid}/${userData.sid}`).json(); 

    if( !tokenStatus.valid ) {
        res.status(401).end(JSON.stringify({ error: 'invalid token' }));
        return true;
    }

    return false;
};

const addBBVAHeaders = (req, headers) => {
    const userData = JSON.parse(headers['proxy-authorization']);

    headers['X-BBVA-UserIP'] = req.ip;
    headers['X-BBVA-UserId'] = userData['uid'];
    headers['X-BBVA-SessionId'] = userData['sid'];
    headers['X-BBVA-UserEmail'] = userData['email'];
    
    delete headers['authorization'];
    delete headers['proxy-authorization'];

    return headers;
};

const shouldCompress = (req, res) => {
    if (req.headers['x-no-compression']) {
      return false;
    }
    return compression.filter(req, res);
  };

const _hooks = {
    onRequest: (req, res) => multipleHooks(req, res, enforceAuthorization),
    rewriteRequestHeaders: addBBVAHeaders
};

const app = express();

app.options('*', cors())

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Authorization, Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

const server = gateway({
    server: app,
    middlewares: [
        cors(),
        require('helmet')(),
        compression({
            filter: shouldCompress,
            threshold: 0,
            level: 7,
        })
    ],
    routes: [
        {
            prefix: '/i',
            target: `${sessionsAPI}`,
            methods: ['POST'],
            hooks: {
                async onResponse (req, res, stream) {  

                    if( (res.statusCode = stream.statusCode) != 200 ) {
                        res.setHeader('content-length', '0');
                        res.end();
                        return;
                    }

                    const resBuffer = Buffer.concat(await toArray(stream));
                    const payload = JSON.parse(resBuffer);
                    const token = jwt.sign({ 
                        uid: payload.id,
                        sid: payload.ssn,
                        email: payload.email
                    }, 'shhhhh', { 
                        expiresIn: 1800 + 's',
                        algorithm: "HS256" 
                    });
                    const response = {
                        'token': token,
                        'id': payload.id,
                        'sid': payload.ssn,
                        'email': payload.email
                    };
                    var _response = "";

                    _response = JSON.stringify(response);

                    res.setHeader('content-length', '' + Buffer.byteLength(_response));
                    
                    res.end(_response);
                }
            }
        },
        {
            prefix: '/o',
            target: `${sessionsAPI}`,
            methods: ['GET'],
            hooks: _hooks
        },
        {
            prefix: '/users',
            target: `${usersAPI}`,
            hooks: _hooks
        },
        {
            prefix: '/bmx',
            target: 'https://www.banxico.org.mx',
            hooks: {
                onRequest: (req, res) => multipleHooks(req, res, 
                    enforceAuthorization, function(req, res) {
                        req.headers['Bmx-Token'] = `${bmxToken}`
                    })
            }
        },
        {
            prefix: '/accounts',
            target: `${accountsAPI}`,
            hooks: _hooks
        }
    ]
});

const https = require('https');
const httpsServer = https.createServer(credentials, app);

httpsServer.listen(PORT, () => {
    console.log(`API Gateway ready on port: ${PORT}`);
});

server.listen(8888, () => {
    console.log(`API Gateway ready on port: 8888`);
})

export default httpsServer
