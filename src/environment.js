class Environment {

    constructor(propertiesReader) {
        var props = propertiesReader('./env/all.properties', 'utf-8', { allowDuplicateSections: true })
        process.argv.forEach(function(val, index, array) {
            let arg = val.split("=");
            if( (arg.length > 0) && (arg[0] === 'env') ) {
                props.append('./env/' + arg[1] + '.properties', 'utf-8', { allowDuplicateSections: true })
            }
        });
        this.properties = props;
    }

    get props() {
        return this.proeprties;
    }

    getProperty(key) {
        return this.properties.get(key);
    }

}

export default new Environment(require('properties-reader'));